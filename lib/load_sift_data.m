function [ SiftData ] = load_sift_data( in_folder,config )
%LOAD_SIFT_DATA Load sift data

listing = dir(in_folder);
% progbar = progressbar_init(config.progressbar_length, length(listing));
cnt = 0;
for i = 1:length(listing)
    if(listing(i).isdir == 0)
        cnt = cnt+1;
        SiftData(cnt) = load(sprintf('%s%s',in_folder,listing(i).name));
    end
    %progbar = progressbar_step(progbar);
end

end

