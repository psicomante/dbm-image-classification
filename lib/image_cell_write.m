function ImageData = image_cell_write(ImageDataSrc, ImageNames, folder)
% writes images to folder
% ImageDataSrc: 1xN cell containing RxC matrices
% ImageName: 1xN cell containing string
% folder: with trailing slash

% save the rows of image cell in order to know how many images we have
images_n = size(ImageDataSrc,2);
progbar = progressbar_init(50,images_n);

for img_idx = 1:images_n
    % image matrix
    I = cell2mat(ImageDataSrc(img_idx));
    
    % image name, loaded from image_cell_load second output parameter
    image_name =  char(ImageNames(img_idx));
    
    % write image
    imwrite(I, strcat(folder, image_name));
    progbar = progressbar_step(progbar);
end

