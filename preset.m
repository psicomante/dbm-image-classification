function [ config ] = preset( function_name,function_desc,opts )
% function [ config ] = preset( function_name,function_desc,opts )
%   Definisce uno standard per la sintassi di invocazione degli script, 
%   si smazza tutte le beghe relative al rispetto degli standard e 
%   restituisce un oggetto di configurazione per l'esecuzione degli script.
%
% function_name: Il nome della funzione che chiama il preset
% function_desc: Una descrizione della funzione (serve per scrivere l'help)
% opts: le opzioni con cui viene chiamata la funzione. Lo standard �:
% <command> [preset_name][--help|-h][--force|-f]
%    preset_name: carica il preset indicato. Se non presente carica quello
%    di default
%    --help (-h): stampa un messaggio di aiuto
%    --force (-f): sovrascrive sempre l'output senza chiedere nulla

if(nargin ~= 3)
    error('wrong number of arguments for function preset. see [help preset] for help');
end

% Usage
usage = sprintf('%s: %s', function_name, function_desc);
usage = sprintf('%s\nUSAGE: %s [preset_name][--force|-f][--help|-h]', usage, function_name);
usage = sprintf('%s\n   preset_name: carica il preset indicato. Se non presente carica quello\ndi default', usage);
usage = sprintf('%s\n   --help (-h): stampa questo messaggio', usage);
usage = sprintf('%s\n   --force(-f): sovrascrive sempre l''output senza chiedere\n', usage);

% Create an empty config struct
config = struct;
config.exit = true;

% PARSE INPUT

args = length(opts);

% preset_name option
preset_name = 'default';
if (~isempty(opts))
    f = opts{1};
    if(~(strcmp(f,'-h')||strcmp(f,'--help')||strcmp(f,'-f')||strcmp(f,'--force')))
        preset_name = f;
        args = args-1;
    end
end

% --help option
if sum(ismember(opts, '-h')) || sum(ismember(opts, '--help'))
    fprintf(usage);
    return;
end

% --force option
if sum(ismember(opts, '-f')) || sum(ismember(opts, '--force'))
    config.force = true;
    args = args-1;
else
    config.force = false;
end

% check number of arguments
if(args ~= 0)
    error('Wrong number of arguments for function %s.\n%s', function_name, usage);
end
    
% Load the default preset
addpath('config');
default_preset;

% Load custom preset if needed
fn = sprintf('%s_preset', preset_name);
if(exist(fn) == 2)
    eval(fn);
else
    warning('[preset:none][script:preset] The preset %s doesn''t exist. Loaded default preset', preset_name);
end

%set config labels for prints
config.labels = sprintf('[preset:%s][script:%s] ', preset_name, function_name);

%add required paths
for i=1:numel(config.addpaths)
    addpath(config.addpaths{i});
end

config.preset = preset_name;
config.exit = false;
end

