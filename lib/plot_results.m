function plot_results(presets, type, titleI, labelsI)
% Plotta i risultati in un grafico a barre
% preset: array di preset i cui risultati vengono mostrati in una o più barre
% type: identifica quale gruppo di risultati mostrare (per ora funziona solo total)
% titleI: titolo del grafico
% labelsI: array di etichette per identificare il preset (es. il preset clustabs100 potrebbe avere etichetta '100 Clusters')

if (nargin < 3)
    titleI = '';
end

if (nargin < 2)
    type = 'total';
end

% loading 'total' presets results
for i=1:length(presets)
    result = load_results(presets{i});
    results{i} = result;

    %numero di categorie presenti nei risultati
    n_cat = length(result.classes);

    %labels{i} = cell2mat(result.classes{n_cat}(1));
    labels{i} = presets{i};
    values(1,i) = cell2mat(result.classes{n_cat}(2));
end

% use the preset name label if not specified
if (nargin > 3 && length(labelsI))
    labels = labelsI;
end

figure
s1 = subplot(1,1,1);
%s2 = subplot(2,1,2);

b = bar(values,'EdgeColor',[0.3,0.3,0.3]);
% text(x,values,num2str(values,'%0.2f'),...
% 'HorizontalAlignment','center',...
% 'VerticalAlignment','bottom')
title(s1,titleI);
%legend(s1,'Correctly Matched');
set(s1,'XTickLabel',labels);

x_loc = get(b, 'XData');
y_height = get(b, 'YData');
arrayfun(@(x,y) text(x-0.1, y+0.05,num2str(y*100,'%0.1f%%'), 'Color', 'r', 'Parent', s1), x_loc, y_height);
