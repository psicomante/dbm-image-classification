function folder = check_folder( folder, in_out, config )
%CHECK_FOLDERS 
%   Esegue le operazioni di controllo delle cartelle necessarie per gli
%   script.

if(strcmp(in_out, 'input'))
    folder = sprintf('%s%s/%s',config.data_folder,folder,config.(sprintf('%s_folder', folder)));
    % fail if nonexistent src directory
    exist_dir_or_fail(folder);
elseif(strcmp(in_out, 'output'))
    folder = sprintf('%s%s/%s',config.data_folder,folder,config.(sprintf('%s_folder', folder)));
    
    % if --force option was used
    if(config.force)
        % delete a dir if it exists
        delete_dir_if_exist(folder);
    else
        % ask if the user wants to overwrite existing out folder
        %delete_dir_if_exist_prompt(folder);
        exist_dir_and_fail(folder);
    end
else
    error('unexpected value of in_out parameter. Allowed: ''input'' or ''output''');
end