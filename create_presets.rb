kmeans_cluster_n = [0.1, 0.2, 0.4, 0.8, 1.6, 3.2, 6.4, 10]
img_width = [120, 240, 480]
ktop = [1, 2, 3, 4, 5]
class_distance = ["euclidean", "cosine", "cityblock"]

Dir.foreach('./config/') do |item|
  next if item == '.' or item == '..' or item == 'default_preset.m'
  File.delete('./config/' << item)
end

class_distance.each { |d| kmeans_cluster_n.each { |km| img_width.each { |w| ktop.each { |kt|
  f = File.open("./config/W" << w.to_s.gsub('.', 'p') << "KM" << km.to_s.gsub('.', 'p') << "D" << d << "KT" << kt.to_s.gsub('.', 'p') << "_preset.m", "w")
  f.puts "config.class_distance = '" << d << "';"
  f.puts "config.kmeans_cluster_n = " << km.to_s << ";"
  f.puts "config.kmeans_cluster_m = 'relative_kp';"
  f.puts "config.img_width = " << w.to_s << ";"
  f.puts "config.ktop = " << kt.to_s << ";"
  f.puts "config.compiled_folder = 'W" << w.to_s << "/';"
  f.puts "config.dataset_folder = 'W" << w.to_s << "/';"
  f.puts "config.clusters_folder = 'W" << w.to_s << "KM" << km.to_s << "D" << d << "/';"
  f.puts "config.clusters_vectors_folder = 'W" << w.to_s << "KM" << km.to_s << "D" << d << "/';"
  f.puts "config.results_folder = 'W" << w.to_s << "KM" << km.to_s << "D" << d << "KT" << kt.to_s << "/';"
  f.close
} } } }

dbplot = %Q$addpath('lib/report');

kmeans = [#{ kmeans_cluster_n.map { |e| e.to_s }.join(',') }];
ktop = [#{ ktop.map { |e| e.to_s }.join(',') }];
width = [#{ img_width.map { |e| e.to_s }.join(',') }];
distance = {#{ class_distance.map { |e| "'" << e << "'" }.join(',') }};
classes_n = 4; % 3 classes + the total

for class_cnt=1:classes_n
  figure('units','normalized','outerposition',[0 0 1 1]);
  p = 1;
  for w=1:length(width)
      for d=1:length(distance)
        result = zeros(length(kmeans),length(ktop));
        for km=1:length(kmeans)
          for kt=1:length(ktop)
            stats_file = sprintf('./data/results/W%dKM%0.5gD%sKT%d/stats.mat', width(w), kmeans(km), distance{d}, ktop(kt));
            stats = load(stats_file);
            total = stats.classes{class_cnt};
            set(gcf,'name',total{1},'numbertitle','off');
            result(km,kt) = total{2};
          end
        end
        t = sprintf('img_width: %d, distance metric: %s', width(w), distance{d});
        hAx(1) = subplot(length(width),length(distance),p);
        surf(ktop,kmeans,result,'FaceColor', 'interp');
        axis([ktop(1) ktop(end) kmeans(1) kmeans(end) 0 1]);
        xlabel('ktop_n');
        ylabel('%_0 of clusters');
        zlabel('% matched');
        caxis([0 1]);
        title(t);
        p = p+1;
      end
  end
end$

f = File.open("./dbplot.m", "w")
f.puts dbplot
f.close
