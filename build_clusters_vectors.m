function build_clusters_vectors(varargin)
% BUILD_CLUSTERS_VECTORS
% use build_clusters_vectors --help or build_clusters_vectors -h to get help

config = preset('build_clusters_vectors', 'create a clusters vector matrix', varargin);
if config.exit
    return;
end

% check folders
dataset_folder = check_folder('dataset', 'input', config);
in_folder = check_folder('clusters', 'input', config);
out_folder = check_folder('clusters_vectors', 'output', config);

% Load sift data from file
fprintf(config.labels);
fprintf('Loading sift data from files...\n');
SiftData = load_sift_data(dataset_folder, config);

% Load clusters
fprintf(config.labels);
fprintf('Loading clusters data from files...\n');
load(sprintf('%sdata',in_folder));
% loading all keypoints-image match (each row is a image id)
kp_img = cluster_universe(:,1);
% loading all keypoints-cluster match (each row is a cluster id)
kp_clust = cluster_universe(:,2);

% numero totale di clusters
n_cluster = length(unique(kp_clust));
% numero totale di immagini nel dataset
n_images = length(SiftData);
% cluster vectors matrix
cluster_vector = zeros(1,n_cluster);

%fprintf('# clusters %d \t # images %d \t \n', n_cluster, n_images);
%fprintf('-- end global -- \n');

% Create clusters_vectors
fprintf(config.labels);
fprintf('Creating clusters vectors...\n');
progbar = progressbar_init(config.progressbar_length, n_images);
% for each image
for i = 1:n_images
    
    % numero totale di keypoints nell'immagine
    tot_kp = count_img_kp(SiftData, i);
    
    %fprintf('# kp %d\n', tot_k);

    % for each cluster
    for j = 1:n_cluster
        
        % numero di keypoints dell'immagine i appartenenti al cluster j
        k_j = get_cluster_metadata(SiftData, kp_img, kp_clust, i, j, 'count_kp_img_cluster');
        
        % cluster frequency 
        % (percentuale di punti di quella immagine che sono stati mappati nel cluster j) 
        df_j = k_j / tot_kp;
        
        
        % numero di immagini nel dataset che hanno almeno un keypoint nel cluster j
        tot_img_j = get_cluster_metadata(SiftData, kp_img, kp_clust, i, j, 'count_images_cluster');
        
        % inverse image frequency 
        % (logaritmo del rapporto tra la cardinalit? del database e il numero di immagini in cui i descrittori mappati in quel cluster sono presenti).
        idf_j = log(n_images / tot_img_j);
        
        % peso del cluster j all'interno dell'imagine i;
        w_j = df_j * idf_j;
        
        % save w_j in clusters_vector
        cluster_vector(j) = w_j;
        
        %fprintf(debug, 'image', i, 'cluster', j, df_j, idf_j);
        %fprintf('image %d \t cluster %d \t \n', i, j);
        %fprintf('df %8d \t k_j %8d \t tot_k %8d \n', df_j, k_j, tot_k);
        %fprintf('-- end cluster -- \n');
    end
    
    %fprintf('-- end image -- \n');
    
    % Save to file!
    SiftData(i).cluster_vector = cluster_vector;
    progbar = progressbar_step(progbar);
end

fprintf(config.labels);
fprintf('saving to file...\n');
exist_dir_or_create(out_folder);

for i=1:length(SiftData)
    s = SiftData(i);
    save(sprintf('%s%s.mat',out_folder,s.name), '-struct', 's');
end
