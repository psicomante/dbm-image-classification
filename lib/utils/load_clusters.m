function [kp_img, kp_clust] = load_clusters(varargin)
% LOAD_CLUSTERS
% use load_clusters --help or load_clusters -h to get help

config = preset('load_clusters', 'load the clusters from file', varargin);
if config.exit
    return;
end

% check folders
in_folder = check_folder('clusters', 'input', config);

% Load clusters
fprintf(config.labels);
fprintf('Loading clusters data from files...\n');
load(sprintf('%sdata',in_folder));
kp_img = cluster_universe(:,1);
kp_clust = cluster_universe(:,2);