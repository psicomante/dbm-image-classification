function delete_dir_if_exist( dir )
%DELETE_DIR_IF_EXIST delete a directory if it exists

if exist(dir, 'dir')
    rmdir(dir, 's');
end

