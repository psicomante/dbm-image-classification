clear;

presets = {'imagesize080', 'imagesize160', 'imagesize320', 'imagesize640', 'imagesize800'};
titles = {'80px width', '160px width', '320px width', '640px width', '800px width'};

figure

for i=1:length(presets)
  sp = subplot(5,1,i);

  stats = load_results(presets{i});

  for i = 1:length(stats.classes)
      labels{i} = cell2mat(stats.classes{i}(1));
      values(1,i) = cell2mat(stats.classes{i}(2));
  end

  b = bar(sp,values,'EdgeColor',[0.3,0.3,0.3]);
  title(sp,titles{i});
  set(sp,'XTickLabel',labels);

  x_loc = get(b, 'XData');
  y_height = get(b, 'YData');
  arrayfun(@(x,y) text(x-0.1, y+0.05,num2str(y*100,'%0.1f%%'), 'Color', 'r', 'Parent', sp), x_loc, y_height);

end
