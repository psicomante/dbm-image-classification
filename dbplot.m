addpath('lib/report');

kmeans = [0.1,0.2,0.4,0.8,1.6,3.2,6.4,10];
ktop = [1,2,3,4,5];
width = [120,240,480];
distance = {'euclidean','cosine','cityblock'};
classes_n = 4; % 3 classes + the total

for class_cnt=1:classes_n
  figure('units','normalized','outerposition',[0 0 1 1]);
  p = 1;
  for w=1:length(width)
      for d=1:length(distance)
        result = zeros(length(kmeans),length(ktop));
        for km=1:length(kmeans)
          for kt=1:length(ktop)
            stats_file = sprintf('./data/results/W%dKM%0.5gD%sKT%d/stats.mat', width(w), kmeans(km), distance{d}, ktop(kt));
            stats = load(stats_file);
            total = stats.classes{class_cnt};
            set(gcf,'name',total{1},'numbertitle','off');
            result(km,kt) = total{2};
          end
        end
        t = sprintf('img_width: %d, distance metric: %s', width(w), distance{d});
        hAx(1) = subplot(length(width),length(distance),p);
        surf(ktop,kmeans,result,'FaceColor', 'interp');
        axis([ktop(1) ktop(end) kmeans(1) kmeans(end) 0 1]);
        xlabel('ktop_n');
        ylabel('%_0 of clusters');
        zlabel('% matched');
        caxis([0 1]);
        title(t);
        p = p+1;
      end
  end
end
