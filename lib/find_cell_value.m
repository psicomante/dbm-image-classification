function index = find_cell_value(cell, value)

index = find(~cellfun('isempty',strfind(cell,value)));