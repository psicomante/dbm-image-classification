function data = get_cluster_metadata(SiftData, keypoints_images, keypoints_cluster, i, j, type)
% get data associated with a cluster or an image in the cluster
% type parameters
% 'count_keypoints_cluster': count the keypoints in the cluster J
% 'count_images_cluster': count the different images whose keypoints are in
% the cluster J
% 'count_kp_img_cluster': count il numero di keypoints dell'immagine i
% classificati come appartenenti al cluster j

% SiftData - struct data of sift data of each image
% keypoints_images - vector of keypoints universe (index) matching the
% owner image (value)
% keypoints_cluster - vector of keypoint and its cluster
% cluster_id - cluster_id whose image list will be calculated

% look for every keypoint contained in cluster_id
kp_ids = find(keypoints_cluster == j);

% look for image owner for every keypoint in kp_ids
% image whose keypoints were clustered as cluster_id
image_kp_values = keypoints_images(kp_ids);

% output = count keypoints in the cluster J
if (strcmp(type, 'count_keypoints_cluster') == 1)
    data = length(kp_ids);
end

% output = count different images in cluster_id
% count unique images in the cluster j
if (strcmp(type, 'count_images_cluster') == 1)
    data = length(unique(image_kp_values));
end

% output = numero di keypoints dell'immagine i mappati nel cluster j
if (strcmp(type, 'count_kp_img_cluster'))
    data = length(find(image_kp_values == i));
end