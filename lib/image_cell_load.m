% a 1xN cell containing image matrix and image data
% image matrix, the actual image
% image data, other info about image, eg. name, category, etc.
function [ImageMatrix,ImageData] = image_cell_load(folder)
% folder: with trailing slash
% folder = './imageset/src/';

% reading image from specific folders (read above)
image_list = dir([folder '*.jpg']);

% preallocate matrices to improve speed
ImageMatrix = cell(1,length(image_list));
ImageData = cell(1,length(image_list));

progbar = progressbar_init(50, length(image_list));


for img_idx = 1:length(image_list)
    % concatenate string with []
    ImageMatrix{img_idx} = im2double(imread([folder image_list(img_idx).name])); 
    
    % @TODO: strip the number
    ImageData{img_idx} = image_list(img_idx).name;
    
    progbar = progressbar_step(progbar);
end