function [stats] = load_results(varargin)
% LOAD_RESULTS
% use load_results --help or load_results -h to get help

config = preset('load_results', 'load the classification results from file', varargin);
if config.exit
    return;
end

% check folders
in_folder = check_folder('results', 'input', config);

% Load Stats
fprintf(config.labels);
fprintf('Loading results data from files...\n');
load(sprintf('%sstats',in_folder));

stats = struct;
stats.preset = config.preset;

stats.classes = classes;
% Numero di cluster
stats.n_clusters = n_clusters;
% Numero di immagini
stats.n_images = n_images;
% Algoritmo usato per il clustering
stats.cluster_alg = cluster_alg;
% Funzione distanza utilizzata durante il clustering
stats.kmeans_distance = kmeans_distance;
% Dunzione distanza utilizzata per la classificazione
stats.class_distance = class_distance;
% Altezza delle immagini
stats.img_height = img_height;
% Larghezza delle immagini
stats.img_width = img_width;
% Preprocessing eseguito sulle immagini in fase di compilazione
stats.preprocess = preprocess;
% Numero di immagini del training set
stats.n_training_set = n_training_set;
% Metodo utilizzato per definire i primi cluster
stats.kmeans_start = kmeans_start;
