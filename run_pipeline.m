function run_pipeline(varargin)
% run_pipeline
% use run_pipeline --help or run_pipeline -h to get help

config = preset('RUN_PIPELINE', 'run all the whole pipeline for the given preset', varargin);
if config.exit
    return;
end

if ~config.force
  folder = sprintf('%s%s/%s',config.data_folder,'compiled',config.compiled_folder);
  if exist(folder, 'dir')
    fprintf(config.labels);
    fprintf('One or more output folders already exist. RESUMING the pipeline.\n');
    fprintf(config.labels);
    fprintf('NB: if you want to overwrite old results, run the script with the option --force\n')
  end
end

fprintf('%stask started!\n', config.labels);

fprintf('%sstarting ''compile'' script...\n', config.labels);
if config.force
  compile(config.preset, '--force');
else
  try
    compile(config.preset);
  catch err
    if strcmp(err.identifier, 'OutFolder:exist')
      fprintf(config.labels);
      fprintf('SKIPPING SCRIPT: out folder already exists!');
    else
      rethrow(err);
    end
  end
end

fprintf('%sstarting ''build_dataset'' script...\n', config.labels);
if config.force
  build_dataset(config.preset, '--force');
else
  try
    build_dataset(config.preset);
  catch err
    if strcmp(err.identifier, 'OutFolder:exist')
      fprintf(config.labels);
      fprintf('SKIPPING SCRIPT: out folder already exists!');
    else
      rethrow(err);
    end
  end
end

fprintf('%sstarting ''build_clusters'' script...\n', config.labels);
if config.force
  build_clusters(config.preset, '--force');
else
  try
    build_clusters(config.preset);
  catch err
    if strcmp(err.identifier, 'OutFolder:exist')
      fprintf(config.labels);
      fprintf('SKIPPING SCRIPT: out folder already exists!');
    else
      rethrow(err);
    end
  end
end

fprintf('%sstarting ''build_clusters_vectors'' script...\n', config.labels);
if config.force
  build_clusters_vectors(config.preset, '--force');
else
  try
    build_clusters_vectors(config.preset);
  catch err
    if strcmp(err.identifier, 'OutFolder:exist')
      fprintf(config.labels);
      fprintf('SKIPPING SCRIPT: out folder already exists!');
    else
      rethrow(err);
    end
  end
end

fprintf('%sstarting ''classification'' script...\n', config.labels);
if config.force
  classification(config.preset, '--force');
else
  try
    classification(config.preset);
  catch err
    if strcmp(err.identifier, 'OutFolder:exist')
      fprintf(config.labels);
      fprintf('SKIPPING SCRIPT: out folder already exists!');
    else
      rethrow(err);
    end
  end
end
fprintf('%stask terminated succesfully!\n', config.labels);