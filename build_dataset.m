function build_dataset(varargin)
% BUILD_DATASET
% use build_dataset --help or build_dataset -h to get help

config = preset('build_dataset', 'extract features with SIFT', varargin);
if config.exit
    return;
end

% check folders
in_folder = check_folder('compiled', 'input', config);
out_folder = check_folder('dataset', 'output', config);

% Load images
fprintf(config.labels);
fprintf('Loading compiled images...\n');
[imageData, imageNames] = image_cell_load(in_folder);


% SIFT, feature (keypoints) extraction
fprintf(config.labels);
fprintf('Extracting features with SIFT...\n');
images_n = size(imageData,2);
progbar = progressbar_init(config.progressbar_length, images_n);
id=0;

dataset = [];

features_n = 0;
for img_idx = 1:images_n
    
    % se l'immagine corrente appartiene al vettore delle ignorate salta
    if (ismember(imageNames(img_idx), config.ignore_images))
        progbar = progressbar_step(progbar);
        continue;
    end
   
    
    ts_flag = 0;

    % from example: [frames,descriptors] = sift(I, 'Verbosity', 1) ;
    [frames,descriptors] = vl_sift(single(cell2mat(imageData(img_idx))));
    
    % Get the category from filename 
    category = strsplit(char(imageNames(img_idx)),'_');
    
    % Ignora se l'immagine corrente appartiene ad una categoria ignorata
    if (~ismember(category(1), config.image_categories))
        progbar = progressbar_step(progbar);
        continue;
    end    
    
    id = id + 1;

    % Set the training_set flag
    if sum(ismember(config.training_set, char(imageNames(img_idx))))
        ts_flag = 1;
    end

    s = struct(...
        'id', id, ...
        'frames', frames,...
        'descriptors', double(descriptors),...
        'name', imageNames(img_idx),...
        'category', category(1),...
        'training_set', ts_flag);
    dataset = [dataset s];
    
    progbar = progressbar_step(progbar);
end

fprintf(config.labels);
fprintf('Saving to file...\n');
exist_dir_or_create(out_folder);

for i=1:length(dataset)
    s = dataset(i);
    save(sprintf('%s%s.mat',out_folder,s.name), '-struct', 's');
end