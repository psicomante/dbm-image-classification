function exist_dir_or_create( dir_name )
% function exist_dir_or_create( dir_name )
%   check if dir_name exists or create

if ~exist(dir_name, 'dir')
    mkdir(dir_name);
end

end

